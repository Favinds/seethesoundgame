﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversHandler : MonoBehaviour
{
    [SerializeField] GameObject[] conversations;

    PopUpControllerDarat parentController;
    int index = 0;

    public void setParentController(PopUpControllerDarat parentController)
    {
        this.parentController = parentController;
    }

    public void startConvers()
    {
        Debug.Log("ConversHandler Started");

        GameObject conversNow = conversations[index];
        conversNow.SetActive(true);

        Button[] childs = conversNow.GetComponentsInChildren<Button>();
        Button nextButton = null, backButton = null;

        foreach (var child in childs)
        {
            if (child.gameObject.CompareTag("Next"))
                nextButton = child;
            else if (child.gameObject.CompareTag("Back"))
                backButton = child;
        }

        if (nextButton != null)
        {
            nextButton.onClick.RemoveAllListeners();
            nextButton.onClick.AddListener(onNext);
        }

        if (backButton != null)
        {
            backButton.onClick.RemoveAllListeners();
            backButton.onClick.AddListener(onBack);
        }
    }

    public void EndConvers()
    {

        gameObject.SetActive(false);
    }

    public void onNext()
    {
        conversations[index].SetActive(false);

        if (index + 1 < conversations.Length)
            ++index;
        else
        {
            parentController.nextConvers();
            return;
        }

        conversations[index].SetActive(true);

        Debug.Log($"duarr next: {index}");

        startConvers();
    }

    public void onBack()
    {
        conversations[index].SetActive(false);

        if (index - 1 >= 0)
            --index;

        conversations[index].SetActive(true);

        Debug.Log($"duarr back: {index}");

        startConvers();
    }
}
