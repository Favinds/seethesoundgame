﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public float speed;
    float dirX;
    public GameObject endPanel;

    private Rigidbody2D rb;
    private Vector2 moveVelocity;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveVelocity = moveInput.normalized * speed;
    } 

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.gameObject.tag == "Move")
            {
                SceneManager.LoadScene("Darat");
            }
            if(collision.gameObject.tag == "End")
        {

        }
    }
  
}
 
