﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpControllerDarat : MonoBehaviour
{
    [SerializeField] GameObject[] animalsConvers;

    GameObject conversNow;
    ConversHandler handler;
    int conversIndex = 0;

    private void OnTriggerEnter(Collider other)
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //    init();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (collision.gameObject.CompareTag("babi"))
                conversIndex = 0;
            else if (collision.gameObject.CompareTag("tikus"))
                conversIndex = 1;

            init();
        }
            
        //if(collision.gameObject.tag == "babi" && Input.GetKeyDown(KeyCode.Space))
        //{
        //    babiConvers.SetActive(true);
        //}
        //if (collision.gameObject.tag == "tikus" && Input.GetKeyDown(KeyCode.Space))
        //{
        //    tikusConvers.SetActive(true);
        //}
        //if (collision.gameObject.tag == "sapi" && Input.GetKeyDown(KeyCode.Space))
        //{
        //    sapiConvers.SetActive(true);
        //}
        //if (collision.gameObject.tag == "macan" && Input.GetKeyDown(KeyCode.Space))
        //{
        //    macanConvers.SetActive(true);
        //}

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(handler!=null)
        {
            handler.EndConvers();
        }
        
    }

    void init()
    {
        conversNow = animalsConvers[conversIndex];
        conversNow.SetActive(true);

        handler = conversNow.GetComponent<ConversHandler>();
        handler.setParentController(this);
        handler.startConvers();
    }

    public void nextConvers()
    {
        conversNow.SetActive(false);
        conversIndex++;
    }
}
